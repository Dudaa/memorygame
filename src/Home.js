import React, { Component } from 'react';
import './App.css';

class Home extends Component {
  constructor(props) {
    super(props);
    this.props = props
    this.state = { pourcentage: 0,end: false };
  }
  loading = ()=>{
    if(this.state.pourcentage<100){
      let add = this.state.pourcentage + parseInt(Math.random()*25) 
      this.setState(state => ({
        pourcentage: (add>100)?100:add
      }));
    }
    else
      this.setState(state => ({
        end: true
      }));
  }

  componentDidMount() {  
      this.interval = setInterval(() => this.loading(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
        <div>
          <p>
            Loading <code>MemoryGame</code> Please wait ...
          </p>
          <p style={{fontSize: "15px"}}>Description: Click on the boxes and find the number and its opposite (the boxes by two) <br /> Note: The game has a countdown timer</p>

          <div className="progress" style={{width: "100%",height: "30px"}}>
            <div className="progress-bar progress-bar-striped bg-warning font-weight-bold" role="progressbar" style={{width: this.state.pourcentage + "%",height: "30px"}} aria-valuenow={this.state.pourcentage} aria-valuemin="0" aria-valuemax="100">{this.state.pourcentage + "%"}</div>
          </div>

          {
            (this.state.end)? <button onClick={this.props.handleStart} className="btn btn-warning btn-big bg-theme mt-2 col-md-4 font-weight-bold">Start</button>:null
          }
        </div>
    );
  }
}

export default Home;
