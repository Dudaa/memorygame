import React, { Component } from 'react';
import Cellule from "./Cellule"
var selected = null;
class Game extends Component {

    constructor(props) {
        super(props);
        this.state = { seconds: 30, score: 0, some:0,index:Array(16).fill(null), cells: Array(16).fill(null) , etat: Array(16).fill(false) };
    }
    componentWillMount() {
        var index=0;
        while(index<16){
            var r=Math.floor(Math.random()*17) - 8;
            if(this.state.cells.indexOf(r)=== -1 && r!==0){
                this.state.cells[index]=r;
                index=index+1;
            }
            
        }

        
    }

    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 1000);
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    tick() {
        if(this.state.seconds>0 )
            this.setState(state => ({
                seconds: state.seconds - 1
            }));
        else
        this.props.gameOver();
       
       
           
    }
    



    responseWrong=(selecte,ind)=>{
        let tmp=this.state.etat;
        tmp[ind]=tmp[selecte]=false;
        this.setState({etat: tmp});
       this.state.etat[selecte]=false;
       this.state.etat[ind]=false;
    console.log("else"+selecte+ind);
    selected=null;
    console.log("else"+selecte+ind);

    }
    changeState =(ind)=>{
        if(this.state.etat[ind]===false){
            let tmp=this.state.etat;
            tmp[ind]=!tmp[ind];
            this.setState({etat: tmp});
            if(selected!==ind && selected!==null){
                console.log(1223);
                
                if(this.state.cells[selected]+this.state.cells[ind]===0){
                    this.state.cells[selected]=this.state.cells[ind]=' '
                    selected=null;
                    
                }else{
                    setTimeout(()=>this.responseWrong(selected,ind), 1000);
                    
                    
                }
            console.log(this.state.etat[ind]);
            
            }
            else selected = ind
        }


    }
    render() {
        return (
            <div className="col-md-6 game">
            
            <span className="float-left">Time {this.state.seconds}</span>

                <table className="table table-bordered">
                <tr>
                        <Cellule ind="0"  value={this.state.cells[0]} etat={this.state.etat[0]} changeState={this.changeState}/>
                        <Cellule ind="1"  value={this.state.cells[1]} etat={this.state.etat[1]} changeState={this.changeState}/>
                        <Cellule ind="2"  value={this.state.cells[2]} etat={this.state.etat[2]} changeState={this.changeState}/>
                        <Cellule ind="3"  value={this.state.cells[3]} etat={this.state.etat[3]} changeState={this.changeState}/>
                    </tr>
                    <tr>
                        <Cellule ind="4"  value={this.state.cells[4]} etat={this.state.etat[4]} changeState={this.changeState}/>
                        <Cellule ind="5"  value={this.state.cells[5]} etat={this.state.etat[5]} changeState={this.changeState}/>
                        <Cellule ind="6"  value={this.state.cells[6]} etat={this.state.etat[6]} changeState={this.changeState}/>
                        <Cellule ind="7"  value={this.state.cells[7]} etat={this.state.etat[7]} changeState={this.changeState}/>
                    </tr>
                    <tr>
                        <Cellule ind="8"  value={this.state.cells[8]} etat={this.state.etat[8]} changeState={this.changeState}/>
                        <Cellule ind="9"  value={this.state.cells[9]} etat={this.state.etat[9]} changeState={this.changeState}/>
                        <Cellule ind="10"  value={this.state.cells[10]} etat={this.state.etat[10]} changeState={this.changeState}/>
                        <Cellule ind="11"  value={this.state.cells[11]} etat={this.state.etat[11]} changeState={this.changeState}/>
                    </tr>
                    <tr>
                        <Cellule ind="12"  value={this.state.cells[12]} etat={this.state.etat[12]} changeState={this.changeState}/>
                        <Cellule ind="13"  value={this.state.cells[13]} etat={this.state.etat[13]} changeState={this.changeState}/>
                        <Cellule ind="14"  value={this.state.cells[14]} etat={this.state.etat[14]} changeState={this.changeState}/>
                        <Cellule ind="15"  value={this.state.cells[15]} etat={this.state.etat[15]} changeState={this.changeState}/>
                    </tr>

                </table>
            </div>
        )
    }

}
export default Game;