import React, { Component } from 'react';

class Cellule extends Component {
    constructor(props) {
        super(props);
        this.props = props
    }
    onClick = () => {
        this.props.changeState(this.props.ind)

        
    }


    render(){
        return(
            <td onClick={this.onClick}>{(this.props.etat===false)?'?':this.props.value}</td>
            )
    }
    
}
export default Cellule;